#!/usr/bin/env node

import * as fs from 'fs';
import * as path from 'path';
import * as yargs from 'yargs';
import * as dotenv from 'dotenv';
import {injectVariables} from './injector';

const argv = yargs
	.usage('Usage: $0 config_var_name [options]')
	.demandCommand(1)
	.option('env', {
		type: 'string',
		array: true,
	})
	.alias('e', 'env')
	.argv;

const envFile = path.join(process.cwd(), '.env');
if (fs.existsSync(envFile) && fs.statSync(envFile).isFile()) {
	dotenv.config({
		path: envFile,
	});
}

let data = '';

setTimeout(() => {
	if (data === '') {
		throw new Error('No file provided via stdin');
	}
}, 100);

process.stdin.on('data', (chunk) => {
	data += chunk;
});

process.stdin.on('end', () => {
	const vars: any = {};

	for (let env of argv.env) {
		const parts = env.split('=');
		if (parts.length === 1) {
			vars[parts[0]] = process.env[parts[0]] || '';
		} else {
			vars[parts[0]] = parts[1];
		}
	}

	process.stdout.write(injectVariables(data, argv._[0], vars));
});

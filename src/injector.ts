import {JSDOM} from 'jsdom';

export function injectVariables(file: string, configName: string, variables: {[name: string]: string}): string
{
	const dom = new JSDOM(file);
	const doc = dom.window.document;

	const configDom = doc.createElement('script');
	configDom.innerHTML = `window.${configName} = ${JSON.stringify(variables)};`;

	doc.head.prepend(configDom);

	return dom.serialize();
}

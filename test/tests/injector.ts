import {expect} from 'chai';
import * as fs from 'fs';
import * as path from 'path';

import {injectVariables} from '../../src/injector';

const readFile = (name: string): string => {
	return fs.readFileSync(path.join(__dirname, '..', 'data', name + '.html'), {encoding: 'utf8'});
};

describe('injectVariables()', () => {

	it('should inject configuration into html file', () => {
		const file = readFile('with_head_original');
		const injected = injectVariables(file, 'env', {
			version: '1.0.0',
		});

		expect(injected).to.be.equal(readFile('with_head_result'));
	});

	it('should inject configuration into html file without head', () => {
		const file = readFile('without_head_original');
		const injected = injectVariables(file, 'env', {
			version: '1.0.0',
		});

		expect(injected).to.be.equal(readFile('without_head_result'));
	});

});
